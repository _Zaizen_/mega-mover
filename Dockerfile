# Abbiamo scelto un SO Debian https://hub.docker.com/_/debian/
FROM python:3

#Parti essenziali per il funzionamento del bot, qui sotto sono principalmente tutte librerie python, ma possiamo notare ffmpeg che serve per convertire i file audio
#RUN apt-get update -y && \
#    apt-get install -y \
#    apt-utils \
#    python3-pip \
#    python3-dev

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
COPY mover.py ./
COPY keys.cfg ./

CMD [ "python", "./mover.py" ]


#In questa fase si copiano i file dentro al container docker
#COPY requirements.txt /tmp/mega_mover_erasmus/
#COPY mover.py /tmp/mega_mover_erasmus/
#COPY keys.cfg /tmp/mega_mover_erasmus/
#imposta la cartella dove il container docker lavorera
#WORKDIR /tmp/mega_mover_erasmus/

#aggiorniamo lel librerie
#RUN pip3 install --upgrade pip setuptools wheel
#RUN pip3 install -r requirements.txt

#ENTRYPOINT [ "python3" ]

#esegue il bot
#CMD [ "mover.py" ]
