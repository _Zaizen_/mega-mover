from mega import Mega
from time import time, sleep

keys = {}

def load_config():
    with open("keys.cfg") as myfile:
        for line in myfile:
            name, var = line.partition("=")[::2]
            keys[name.strip()] = var[:-1]
        myfile.close()

def move_files(files, folder1, folder2, m):
    for file in files:
        if files[file]['p'] == folder1[0] and len(files[file]['a']) > 1:
            m.move(files[file]['h'],folder2[0])

def main():
    print("Carico le configurazioni")
    load_config()
    mega = Mega()
    print("Faccio il login su MEGA")
    m = mega.login(keys["email"], keys["password"])
    #details = m.get_user()
    print("Trovo la prima cartella che mi interessa")
    folder1 = m.find('Creta_Erasmus+')
    print("Trovo la seconda cartella che mi interessa")
    folder2 = m.find('Foto_Video')
    #folder_files = folder.get_files()
    #print(folder)
    #print( folder[0] )
    #print("#############################")
        #print((files[file]))
    counter = 0
    print("Avvio il loop")
    while True:
        if counter == 30:
            print("Faccio il login su MEGA")
            load_config()
            m = mega.login(keys["email"], keys["password"])
            counter = 0
        print("Elenco i file")
        files = m.get_files()
        #print(files)
        print("Eseguo lo spostamento dei files")
        move_files(files, folder1, folder2, m)
        sleep(60 - time() % 60)
        counter = counter + 1

if __name__ == '__main__':
    main()
